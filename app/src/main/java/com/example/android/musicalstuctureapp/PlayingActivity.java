package com.example.android.musicalstuctureapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

public class PlayingActivity extends AppCompatActivity {

    // definitions of the play status
    private final static int NOT_PLAY = 0;
    private final static int PLAYING = 1;
    private final static int PAUSE = 2;

    private enum State {
        NotPlay(NOT_PLAY),
        Playing(PLAYING),
        Pause(PAUSE);

        private final int value;
        State(int value) {
            this.value = value;
        }

        public int getValue() {
            return this.value;
        }
    }

    // current song (assume the first song in the previous list is selected)
    private int mSongIndex = 0;
    // current play state
    private int mPlayState = State.Playing.getValue();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_playing);

        updateDisplayEvent();

        // click this view to display the store view
        TextView store = (TextView) findViewById(R.id.text_view_store);
        store.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // create the StoreActivity intent
                Intent intent = new Intent(PlayingActivity.this, StoreActivity.class);
                startActivity(intent);
            }
        });

        // click my list to stop playing and display my list
        TextView myList= (TextView) findViewById(R.id.text_view_my_list);
        myList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // LibraryActivity intent
                Intent intent = new Intent(PlayingActivity.this, LibraryActivity.class);
                // Let my list show
                intent.putExtra("DisplayMode", LibraryActivity.Mode.MyList.getValue());
                startActivity(intent);
            }
        });

        // click my list to stop playing and display the list of all songs
        TextView allSongs = (TextView) findViewById(R.id.text_view_all_songs);
        allSongs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // LibraryActivity intent
                Intent intent = new Intent(PlayingActivity.this, LibraryActivity.class);
                // Let my list show
                intent.putExtra("DisplayMode", LibraryActivity.Mode.AllSongs.getValue());
                startActivity(intent);
            }
        });

        // click my list to stop playing and display artists list
        TextView artists = (TextView) findViewById(R.id.text_view_artists);
        artists.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // LibraryActivity intent
                Intent intent = new Intent(PlayingActivity.this, LibraryActivity.class);
                // Let my list show
                intent.putExtra("DisplayMode", LibraryActivity.Mode.Artists.getValue());
                startActivity(intent);
            }
        });

        // click my list to stop playing and display albums list
        TextView album = (TextView) findViewById(R.id.text_view_album_list);
        album.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // LibraryActivity intent
                Intent intent = new Intent(PlayingActivity.this, LibraryActivity.class);
                // Let my list show
                intent.putExtra("DisplayMode", LibraryActivity.Mode.Album.getValue());
                startActivity(intent);
            }
        });

        // click previous button to play the previous song
        TextView previous = (TextView) findViewById(R.id.text_view_previous);
        previous.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                previousMusic();
            }
        });

        // click next button to play the next song
        TextView next = (TextView) findViewById(R.id.text_view_next);
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                nextMusic();
            }
        });

        // click onPlay button to stop or resume the playing
        TextView onPlay = (TextView) findViewById(R.id.text_view_stop);
        onPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onPlay();
            }
        });

        // click pause button to pause the playing
        TextView pause = (TextView) findViewById(R.id.text_view_pause);
        pause.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pausePlay();
            }
        });

    }

    /**
     *  Update the message showing when a event happens
     */
    private void updateDisplayEvent() {

        // message of the pause text view
        String pauseMessage = getString(R.string.playing_pause);
        // message of the stop text view
        String stopMessage = getString(R.string.playing_stop);
        // message of the playing state
        String contentMessage = "";

        switch (mPlayState) {
            case PLAYING:
                contentMessage = getString(R.string.playing_playing);
                break;
            case PAUSE:
                contentMessage = getString(R.string.playing_pause);
                pauseMessage = getString(R.string.playing_resume);
                break;
            case NOT_PLAY:
                contentMessage = getString(R.string.playing_stop);
                stopMessage = getString(R.string.playing_start);
                break;
            default:
                break;
        }

        if (!contentMessage.equals("")) {
            contentMessage += getString(R.string.plaing_song_index);
            contentMessage += mSongIndex;
        }

        // show the play state message
        TextView playContent = (TextView) findViewById(R.id.text_view_play_content);
        playContent.setText(contentMessage);
        // update the pause text view message
        TextView pauseTextView = (TextView) findViewById(R.id.text_view_pause);
        pauseTextView.setText(pauseMessage);
        // update the stop text view message
        TextView stopTextView = (TextView) findViewById(R.id.text_view_stop);
        stopTextView.setText(stopMessage);
    }

    /**
     *  the event while stop button is clicked
     */
    private void onPlay() {

        switch (mPlayState) {
            case PLAYING:
                mPlayState = State.NotPlay.getValue();
                break;
            case PAUSE:
                mPlayState = State.NotPlay.getValue();
                break;
            case NOT_PLAY:
                mPlayState = State.Playing.getValue();
                break;
            default:
                return;
        }

        updateDisplayEvent();

    }

    /**
     *  the event while the pause button is clicked
     */
    private void pausePlay() {

        if (mPlayState == State.Playing.getValue()) {
            mPlayState = State.Pause.getValue();
        } else if (mPlayState == State.Pause.getValue()) {
            mPlayState = State.Playing.getValue();
        } else {
            return; // do nothing
        }

        updateDisplayEvent();
    }

    /**
     *  the event while the next button is clicked
     */
    private void nextMusic() {
        mSongIndex += 1;
        mPlayState = State.Playing.getValue();
        updateDisplayEvent();
    }

    /**
     *  the event while the previous button is clicked
     */
    private void previousMusic() {
        if (mSongIndex > 0) {
            mSongIndex -= 1;
            mPlayState = State.Playing.getValue();
            updateDisplayEvent();
        }
    }
}
