package com.example.android.musicalstuctureapp;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

public class LibraryActivity extends AppCompatActivity {

    // definitions of the color
    private final static String TOP_BAR_SELECTED = "#006064";
    private final static String TOP_BAR_NOT_SELECTED = "#00BCD4";

    // definitions of the display mode
    private final static int MY_LIST = 0;
    private final static int ALL_SONGS = 1;
    private final static int ARTISTS = 2;
    private final static int ALBUM = 3;

    enum Mode {
        MyList(MY_LIST),
        AllSongs(ALL_SONGS),
        Artists(ARTISTS),
        Album(ALBUM);

        private final int value;

        Mode(int value) {
            this.value = value;
        }

        public int getValue() {
            return this.value;
        }
    }

    // the display mode
    private int mDisplayMode = Mode.MyList.getValue();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_library);

        Intent initIntent = getIntent();
        mDisplayMode = initIntent.getIntExtra("DisplayMode", Mode.MyList.getValue());

        updateDisplayEvent();

        // click this view to display the store view
        TextView store = (TextView) findViewById(R.id.text_view_store);
        store.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // create the StoreActivity intent
                Intent intent = new Intent(LibraryActivity.this, StoreActivity.class);
                startActivity(intent);
            }
        });

        // short click this view to display the playing view
        TextView songSelected = (TextView) findViewById(R.id.text_view_song_selected);
        songSelected.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // create the PlayingActivity intent
                Intent intent = new Intent(LibraryActivity.this, PlayingActivity.class);
                startActivity(intent);
            }
        });

        // short click this view to display the detail information
        songSelected.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                // create the DetailActivity intent
                Intent intent = new Intent(LibraryActivity.this, DetailActivity.class);
                startActivity(intent);
                return true;
            }
        });

        // click this view to display the songs in my list
        TextView myList = (TextView) findViewById(R.id.text_view_my_list);
        myList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setModeToMyList();
            }
        });

        // click this view to display the list of all songs
        TextView allSongs = (TextView) findViewById(R.id.text_view_all_songs);
        allSongs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setModeToAllSongs();
            }
        });

        // click this view to display the list of artists
        TextView artists = (TextView) findViewById(R.id.text_view_artists);
        artists.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setModeToArtists();
            }
        });

        // click this view to display the lists of albums
        TextView album = (TextView) findViewById(R.id.text_view_album_list);
        album.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setModeToAlbum();
            }
        });
    }

    /**
     *  update the display when a event happens
     */
    private void updateDisplayEvent() {

        String message = "";
        // initialize the top bar background color
        TextView myList = (TextView) findViewById(R.id.text_view_my_list);
        myList.setBackgroundColor(Color.parseColor(TOP_BAR_NOT_SELECTED));
        TextView allSongs = (TextView) findViewById(R.id.text_view_all_songs);
        allSongs.setBackgroundColor(Color.parseColor(TOP_BAR_NOT_SELECTED));
        TextView artists = (TextView) findViewById(R.id.text_view_artists);
        artists.setBackgroundColor(Color.parseColor(TOP_BAR_NOT_SELECTED));
        TextView albums = (TextView) findViewById(R.id.text_view_album_list);
        albums.setBackgroundColor(Color.parseColor(TOP_BAR_NOT_SELECTED));

        switch (mDisplayMode) {
            case MY_LIST:  // My list
                message = getString(R.string.library_wizard_my_list);
                myList.setBackgroundColor(Color.parseColor(TOP_BAR_SELECTED));
                break;
            case ALL_SONGS:  // All songs
                message = getString(R.string.library_wizard_all);
                allSongs.setBackgroundColor(Color.parseColor(TOP_BAR_SELECTED));
                break;
            case ARTISTS:  // Artists list
                message = getString(R.string.library_wizard_artists);
                artists.setBackgroundColor(Color.parseColor(TOP_BAR_SELECTED));
                break;
            case ALBUM:  // Album list
                message = getString(R.string.library_wizard_album);
                albums.setBackgroundColor(Color.parseColor(TOP_BAR_SELECTED));
                break;
            default:
                break;
        }

        message += "\n\n";
        message += getString(R.string.library_song_list);

        TextView wizard = (TextView) findViewById(R.id.text_view_song_selected);
        wizard.setText(message);
    }

    /**
     *  change the display mode to my list
     */
    private void setModeToMyList() {
        this.mDisplayMode = Mode.MyList.getValue();
        updateDisplayEvent();
    }

    /**
     *  change the display mode to all songs
     */
    private void setModeToAllSongs() {
        this.mDisplayMode = Mode.AllSongs.getValue();
        updateDisplayEvent();
    }

    /**
     *  change the display mode to artists list
     */
    private void setModeToArtists() {
        this.mDisplayMode = Mode.Artists.getValue();
        updateDisplayEvent();
    }

    /**
     *  change the display mode to the album list
     */
    private void setModeToAlbum() {
        this.mDisplayMode = Mode.Album.getValue();
        updateDisplayEvent();
    }
}
