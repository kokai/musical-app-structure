package com.example.android.musicalstuctureapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // click this view to display the library view that shows the songs in my list
        TextView myList = (TextView) findViewById(R.id.text_view_my_list);
        myList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // create the libraryActivity intent
                Intent intent = new Intent(MainActivity.this, LibraryActivity.class);
                startActivity(intent);
            }
        });

        // click this view to display the library view that shows all of the songs stored in device
        TextView allSongs = (TextView) findViewById(R.id.text_view_all_songs);
        allSongs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // create the libraryActivity intent
                Intent intent = new Intent(MainActivity.this, LibraryActivity.class);
                startActivity(intent);
            }
        });

        // click this view to display the library view that shows the list of artists
        TextView artists = (TextView) findViewById(R.id.text_view_artists);
        artists.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // create the libraryActivity intent
                Intent intent = new Intent(MainActivity.this, LibraryActivity.class);
                startActivity(intent);
            }
        });

        // click this view to display the library view that shows the list of albums
        TextView albumList = (TextView) findViewById(R.id.text_view_album_list);
        albumList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // create the libraryActivity intent
                Intent intent = new Intent(MainActivity.this, LibraryActivity.class);
                startActivity(intent);
            }
        });

        // click this view to display the store view
        TextView store = (TextView) findViewById(R.id.text_view_store);
        store.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // create the StoreActivity intent
                Intent intent = new Intent(MainActivity.this, StoreActivity.class);
                startActivity(intent);

            }
        });
    }
}
