package com.example.android.musicalstuctureapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

public class DetailActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        // click this view to display the store view
        TextView store = (TextView) findViewById(R.id.text_view_store);
        store.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // create the StoreActivity intent
                Intent intent = new Intent(DetailActivity.this, StoreActivity.class);
                startActivity(intent);
            }
        });

        // click my list to stop playing and display my list
        TextView myList= (TextView) findViewById(R.id.text_view_my_list);
        myList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // LibraryActivity intent
                Intent intent = new Intent(DetailActivity.this, LibraryActivity.class);
                // Let my list show
                intent.putExtra("DisplayMode", LibraryActivity.Mode.MyList.getValue());
                startActivity(intent);
            }
        });

        // click my list to stop playing and display the list of all songs
        TextView allSongs = (TextView) findViewById(R.id.text_view_all_songs);
        allSongs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // LibraryActivity intent
                Intent intent = new Intent(DetailActivity.this, LibraryActivity.class);
                // Let my list show
                intent.putExtra("DisplayMode", LibraryActivity.Mode.AllSongs.getValue());
                startActivity(intent);
            }
        });

        // click my list to stop playing and display artists list
        TextView artists = (TextView) findViewById(R.id.text_view_artists);
        artists.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // LibraryActivity intent
                Intent intent = new Intent(DetailActivity.this, LibraryActivity.class);
                // Let my list show
                intent.putExtra("DisplayMode", LibraryActivity.Mode.Artists.getValue());
                startActivity(intent);
            }
        });

        // click my list to stop playing and display albums list
        TextView album = (TextView) findViewById(R.id.text_view_album_list);
        album.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // LibraryActivity intent
                Intent intent = new Intent(DetailActivity.this, LibraryActivity.class);
                // Let my list show
                intent.putExtra("DisplayMode", LibraryActivity.Mode.Album.getValue());
                startActivity(intent);
            }
        });
    }
}
