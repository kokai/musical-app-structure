package com.example.android.musicalstuctureapp;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

public class StoreActivity extends AppCompatActivity {

    // definitions of the color
    private final static String TOP_BAR_SELECTED = "#C2185B";
    private final static String TOP_BAR_NOT_SELECTED = "#EC407A";

    // the definition of the display modes
    private final static int FEATURE = 0;
    private final static int RANKING = 1;
    private final static int LATEST = 2;

    private enum Mode {
        Feature(FEATURE),
        Ranking(RANKING),
        Latest(LATEST);

        private final int value;
        Mode(int value) {
            this.value = value;
        }

        public int getValue() {
            return this.value;
        }
    }

    // the display mode
    private int mDisplayMode = Mode.Feature.getValue();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_store);

        updateDisplayEvent();

        TextView myLibrary = (TextView) findViewById(R.id.text_view_library);
        myLibrary.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // LibraryActivity intent
                Intent intent = new Intent(StoreActivity.this, LibraryActivity.class);
                // Let my list show
                intent.putExtra("DisplayMode", LibraryActivity.Mode.MyList.getValue());
                startActivity(intent);
            }
        });

        TextView feature = (TextView) findViewById(R.id.text_view_feature);
        feature.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setFeatureMode();
            }
        });

        TextView ranking = (TextView) findViewById(R.id.text_view_ranking);
        ranking.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setRankingMode();
            }
        });

        TextView latest = (TextView) findViewById(R.id.text_view_new_songs);
        latest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setLatestMode();
            }
        });
    }

    /**
     *  update the display when a event happens
     */
    private void updateDisplayEvent() {

        String message = "";
        // initialize the top bar selected color
        TextView feature = (TextView) findViewById(R.id.text_view_feature);
        feature.setBackgroundColor(Color.parseColor(TOP_BAR_NOT_SELECTED));
        TextView ranking = (TextView) findViewById(R.id.text_view_ranking);
        ranking.setBackgroundColor(Color.parseColor(TOP_BAR_NOT_SELECTED));
        TextView newSongs = (TextView) findViewById(R.id.text_view_new_songs);
        newSongs.setBackgroundColor(Color.parseColor(TOP_BAR_NOT_SELECTED));

        switch (mDisplayMode) {
            case FEATURE:
                message = getString(R.string.store_content_feature);
                feature.setBackgroundColor(Color.parseColor(TOP_BAR_SELECTED));
                break;
            case RANKING:
                message = getString(R.string.store_content_ranking);
                ranking.setBackgroundColor(Color.parseColor(TOP_BAR_SELECTED));
                break;
            case LATEST:
                message = getString(R.string.store_content);
                newSongs.setBackgroundColor(Color.parseColor(TOP_BAR_SELECTED));
            default:
                break;
        }

        TextView content = (TextView) findViewById(R.id.text_view_content);
        content.setText(message);
    }

    /**
     *  change the display mode to feature songs
     */
    private void setFeatureMode() {
        mDisplayMode = Mode.Feature.getValue();
        updateDisplayEvent();
    }

    /**
     *  change the display mode to the ranking of the songs
     */
    private void setRankingMode() {
        mDisplayMode = Mode.Ranking.getValue();
        updateDisplayEvent();
    }

    /**
     *  change the display mode to the latest sale songs
     */
    private void setLatestMode() {
        mDisplayMode = Mode.Latest.getValue();
        updateDisplayEvent();
    }
}
